// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDgWJlepA75doPOtKh-e0Orh8DKsbdD9Ls',
    authDomain: 'controle-vinicius-if.firebaseapp.com',
    projectId: 'controle-vinicius-if',
    storageBucket: 'controle-vinicius-if.appspot.com',
    messagingSenderId: '905009285950',
    appId: '1:905009285950:web:ba13d1006e8028c6713a6f',
    measurementId: 'G-DBY3XGCFYY'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
